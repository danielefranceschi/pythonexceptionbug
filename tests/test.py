import unittest
from src.pkg.exceptions import TestExecutionException
from src.pkg import myclass


class TestFailureInCI(unittest.TestCase):

    def test_ok(self):
        res = myclass.callfunc("good", "verygood")
        self.assertEqual(res, "verygood")

    def test_failure(self):
        with self.assertRaises(TestExecutionException):
            _ = myclass.callfunc("bad", "I fail in CI")
