import os
import gitlab
import pprint
from gitlab.v4.objects import *

def dumpjob(prj, job):
    pprint.pp(job.attributes, depth=2)
    j = prj.jobs.get(job.get_id(), lazy=True)
    joblog=j.trace().decode("utf-8").split("\n")
    pprint.pprint(joblog)


gl = gitlab.Gitlab('https://gitlab.com', job_token=os.environ['CI_JOB_TOKEN'])
#gl = gitlab.Gitlab('https://gitlab.com', private_token='xxxxxxxxxxxx')
#gl.auth()

project = gl.projects.get(os.getenv('CI_PROJECT_ID') or 24447492)
pipeline = project.pipelines.get(os.getenv('CI_PIPELINE_ID') or 256997962) 
jobs = pipeline.jobs.list()

lastjob = None
for job in jobs:
    if job.get_id() == os.getenv('CI_JOB_ID'):
        continue
    lastjob=job

if lastjob:
    dumpjob(project, lastjob)
