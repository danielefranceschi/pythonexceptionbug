from .exceptions import TestExecutionException
from .calledclass import CalledClass
from typing import Any


class MyClass():

    def switch_function(self, name:str, param:Any) -> str:
        cc: CalledClass = CalledClass()
        return cc.infer(name, param)


def callfunc(name:str, param: Any) -> str:
    mc: MyClass = MyClass()
    return mc.switch_function(name, param)