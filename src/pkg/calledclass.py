from .exceptions import TestExecutionException
from typing import Any

class CalledClass():

    def _detect_from_good(self, param: Any) -> str:
        return str(param)

    def _detect(self, name:str, param: Any) -> str:
        method_name = '_detect_from_' + name
        if hasattr(self, method_name):
            method = getattr(self, method_name)
            return method(param)
        else:
            raise TestExecutionException(f"failed method lookup: {name}")

    def infer(self, name:str, param: Any) -> str:
        return self._detect(name, param)
